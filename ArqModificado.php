<?php

class ArqModificado{

    /*
    *   DirectoryIterator::__construct — Constrói um novo iterador de diretório a partir de um caminho
    *   DirectoryIterator::current — Retorna $this (necessário para a interface Iterator)
    *   DirectoryIterator::getATime — Retorna a data de último acesso do arquivo
    *   DirectoryIterator::getCTime — Retorna a data de modificação do inode do arquivo
    *   DirectoryIterator::getFilename — Retorna o nome do arquivo do elemento atual do diretório
    *   DirectoryIterator::getGroup — Retorna o grupo do arquivo
    *   DirectoryIterator::getInode — Retorna o inode do arquivo
    *   DirectoryIterator::getMTime — Retorna a data da última modificação do arquivo
    *   DirectoryIterator::getOwner — Retorna o proprietário do arquivo
    *   DirectoryIterator::getPath — Retorna o caminho do diretório
    *   DirectoryIterator::getPathname — Retorna o caminho e o nome do arquivo atual do diretório
    *   DirectoryIterator::getPerms — Retorna as permissões do arquivo
    *   DirectoryIterator::getSize — Retorna o tamanho do arquivo
    *   DirectoryIterator::getType — Retorna o tipo do arquivo
    *   DirectoryIterator::isDir — Retorna true se o elemento atual é um diretório
    *   DirectoryIterator::isDot — Retorna true se o elemento atual for ‘.’ ou ‘..’
    *   DirectoryIterator::isExecutable — Retorna true se o arquivo for executável
    *   DirectoryIterator::isFile — Retorna true se o elemento atual for um arquivo
    *   DirectoryIterator::isLink — Retorna true se o elemento atual for um link simbólico
    *   DirectoryIterator::isReadable — Retorna true se o arquivo pode ser lido
    *   DirectoryIterator::isWritable — Retorna true se o arquivo pode ser modificado
    *   DirectoryIterator::key — Retorna o elemento atual do diretório
    *   DirectoryIterator::next — Avança para o próximo elemento
    *   DirectoryIterator::rewind — Recomeça a iteração do diretório
    *   DirectoryIterator::valid — Verifica se o diretório possui ou não mais elementos
    */

    /**
     * @var date
    */
    private $dataBackup;

    /**
     * @var String
    */
    private $path;

    /**
     * Recebe como parâmetro a partir de qual data será realizada o backup e o caminho da raiz principal do projeto que será percorrido.
     * @param String $dataBackup
     * @param string $path
     * @return void
     */
    public function __construct($dataBackup, $path)
    {
        $this->dataBackup = new DateTime($dataBackup);
        $this->path = $path;
        $this->executar();
    }

    /**
     * Description
     * @return void
     */
    private function executar()
    {
        // Percorre os arquivos, pastas e subpastas recursivamente.
        $it = new RecursiveTreeIterator(new RecursiveDirectoryIterator($this->path));
        $dirs = array();
        // Conta a quantidade total de arquivos
        $count = 0;
        // Conta a quantidade de arquivos modificados
        $countModificados = 0;

        // Recomeça a iteração do diretório
        $it->rewind();

        // Verifica se o diretório possui ou não mais elementos
        while($it->valid()) {
            /*
            * isDot(): Retorna true se o elemento atual for ‘.’ ou ‘..’
            */
            if (!$it->isDot() && $it->isFile()) {
                    $count ++;
                    /*
                    * getMTime(): Retorna a data (timestamp) da última modificação do arquivo
                    * date('Y-m-d', $it->getMTime()): Converte de timestamp para date
                    */
                    $dataModificacao = new DateTime(date('Y-m-d', $it->getMTime()));
                    if($dataModificacao > $this->dataBackup){
                        $countModificados++;
                        $dirs[] = array(
                            // getPath(): Retorna o caminho do diretório
                            'path' => $it->getPath(),
                            'date' => date('d/m/Y', $it->getMTime()),
                            // getFilename(): Retorna o nome do arquivo do elemento atual do diretório
                            'name' => $it->getFilename(),
                        );

                    }
            }
            $it->next();
        }
        $this->mensagem($dirs, $count, $countModificados);
    }

    /**
     * $dirs: Array com os arquivos modificados | $count: Quantidade de arquivos percorridos | $countModificados: Quantidade de arquivos modificados
     * @param Array $dirs
     * @param Integer $count
     * @param Integer $countModificados
     * @return void
    */
    private function mensagem($dirs, $count, $countModificados)
    {
        echo "<pre>";
            print_r($dirs);
        echo "</pre>";
        $tempo_carregamento = mktime() - $_SERVER['REQUEST_TIME'];
        print("Quantidade de arquivos percorridos: {$count}");
        echo "<br/>";
        print("Quantidade de arquivos modificados: {$countModificados} \n");
        echo "<br/>";
        print "A página demorou {$tempo_carregamento} segundos para carregar. \n";

    }
}






